const YAML = require("yaml");
var copydir = require('copy-dir');
const moment = require('moment');
const sysinfo = require("systeminformation");
const chalk = require('chalk');
const Promise = require('promise');
const execSh = require("exec-sh");
const path = require("path");
const doc = new YAML.Document()
const constantes = require("./constantes.json")
//const mbt = require("mbt/bin/mbt");
var fs = require('fs');

doc.version = false
doc.setSchema("1.2")

sysinfo.osInfo(function(data1){
  console.log("sistema operativo \n",data1);
});


function consoleBeginEnd(text, isbegin) {
  if (isbegin) {
    console.log(chalk.white.bgGray.bold("\n\nBEGIN ********** " + text + " ********** BEGIN\n\n"));
  } else {
    console.log(chalk.white.bgGray.bold("\n\nEND ********** " + text + " ********** END\n\n"));
  }
}

function consoleSuccess(message, data) {
  console.log(chalk.white.bgGreen.bold("\nSUCCESS: " + message));
  if (data) {
    console.log(chalk.white.bgGreen.bold(data));
  }

}

function consoleInfo(message) {
  console.log(chalk.bgBlue.bold("\nINFO: " + message + "\n"));
}

function consoleError(message, error) {
  console.log(chalk.bgRed.bold("\nERROR :" + message + "\n"));
  if (error) {
    console.log(chalk.bgRed.bold(error));
  }
}

function copiarCarpeta(origen, destino) {
  // Copiar archivos de la aplicación ui5
  return new Promise(function (success, error) {
    try {
      copydir.sync(origen, destino, {
        filter: function (stat, filepath, filename) {
          if (stat === 'directory' && filename === 'dist') {
            return false;
          }

          if (stat === 'directory' && filename === 'deploy_ci_scp') {
            return false;
          }

          if (stat === 'directory' && filename === '.git') {
            return false;
          }

          if (stat === 'directory' && filename === 'node_modules') {
            return false;
          }

          if (stat === 'file' && filename === '.gitignore') {
            return false;
          }
          // ARCHIVOS COPIADOS
          consoleSuccess(stat + " " + filename + " creado correctamente");
          return true;
        }
      });
    } catch (error) {
      consoleError(error.message);
    }
  });


}

function generarMTAFile(destino, appname) {
  return new Promise(function (resolve, reject) {
    try {

      doc.contents = {
        ID: appname,
        "_schema-version": '2.1',
        version: "1.0.1",
        modules: [{
          name: appname,
          parameters: {
            version: moment().subtract(5, 'h').format("YYYYMMDDHHmmss")
          },
          type: "html5",
          path: destino
        }]
      }
      var mtaString = String(doc);
      consoleInfo("MTAContent\n" + mtaString);
      // writeFile function with filename, content and callback function
      fs.writeFile(destino + '/mta.yaml', mtaString, function (err) {
        if (err) {
          consoleError(err.message, err);
          reject(err);
        } else {
          consoleSuccess(destino + '/mta.yaml creado correctamente');
          resolve();
        }
      });
    } catch (error) {
      consoleError(error.message);
      reject(error);
    }
  });
}


function mbtInit_Mtar(destino) {
  return new Promise(function (resolve, reject) {
    
    execSh("mbt init --source=" + destino + " --target=" + destino, true, function (error, salidaOk, salidaError) {
      if (error) {
        consoleError(error.message, error);
        return reject(error.message);
      }

      if (salidaError) {
        consoleError(salidaError);
        return reject(salidaError);
      }

      consoleInfo(salidaOk);
      consoleSuccess("mbt init ejecutado correctamente");
      return resolve(salidaOk);
    });
  });



}


function mbtBuild_Mtar(destino, nameMtar, plataforma) {
  return new Promise(function (resolve, reject) {
    execSh("mbt build --source=" + destino + " --target=" + destino + " --mtar=" + nameMtar + " --platform=" + plataforma, true, function (error, salidaOk, salidaError) {
      if (error) {
        consoleError(error.message, error);
        return reject(error.message);
      }

      if (salidaError) {
        consoleError(salidaError);
        return reject(salidaError);
      }

      consoleInfo(salidaOk);
      consoleSuccess("mbt build ejecutado correctamente");
      return resolve(salidaOk);
    });
  });

}


function deploy_neo_ui5_mtar(host,account,user,password) {
  console.log(__dirname+"/scpsdk")
  var rutaDirectorioDist = __dirname.replace("ui5-neodeploy","dist")+"/"+constantes.nombreMtar;
  return new Promise(function (resolve, reject) {
    execSh(__dirname+"/scpsdk/neo.sh deploy-mta --source "+rutaDirectorioDist+" --synchronous --host "+host+" --account "+account+" --user "+user+" --password "+password, null, function (error, salidaOk, salidaError) {
      if (error) {
        consoleError(error.message, error);
        return reject(error.message);
      }

      if (salidaError) {
        consoleError(salidaError);
        return reject(salidaError);
      }

      consoleInfo(salidaOk);
      consoleSuccess("mbt build ejecutado correctamente");
      return resolve(salidaOk);
    });
  });

}

exports.build_ui5 = async function (appname) {
    
  consoleBeginEnd("BUILD - 1. copiar carpetas  y archivos", true);
  copiarCarpeta(constantes.origen, constantes.destino).then().catch();
  consoleBeginEnd("BUILD - 1. copiar carpetas  y archivos", false);

  consoleBeginEnd("BUILD - 2. generar y crear archivo mta", true);
  await generarMTAFile(constantes.destino,appname).then().catch();
  consoleBeginEnd("BUILD - 2. generar y crear archivo mta", false);


  consoleBeginEnd("BUILD - 3. ejecutar mbt init", true);
  await mbtInit_Mtar(constantes.destino).then().catch();
  consoleBeginEnd("BUILD - 3. ejecutar mbt init", false);

  consoleBeginEnd("BUILD - 4. ejecutar mbt build", true);
  await mbtBuild_Mtar(constantes.destino, constantes.nombreMtar, constantes.platforma).then().catch();
  consoleBeginEnd("BUILD - 4. ejecutar mbt build", false);
}

exports.deploy_ui5 = async function (host,account,user,password) {
  consoleBeginEnd("DEPLOY - 1. desplegar aplicación UI5 en la plataforma scp neo", true);
  await deploy_neo_ui5_mtar(host,account,user,password);
  consoleBeginEnd("DEPLOY - 1. desplegar aplicación UI5 en la plataforma scp neo", true);
}

require('make-runnable');
